"""
.. module:: settings
   :synopsis: Project constants and definitions.

.. moduleauthor:: Nikola Rankovic <kibernick@gmail.com>
"""

#All avaiable are: ['de', 'en', 'fr', 'nl'] 
LANG_CODES = ['en', 'nl', 'de']
TWEETS_PATH = "twitter/"
TWEETS_NAME = "(lang).txt"
KWORDS_PATH = "words/"
KWORDS_NAME = "top100(lang).txt"
KWORDS_MAX = 100
MAX_WORD_LEN = 100